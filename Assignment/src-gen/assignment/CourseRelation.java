/**
 */
package assignment;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link assignment.CourseRelation#getCourse <em>Course</em>}</li>
 *   <li>{@link assignment.CourseRelation#getCreditsReduction <em>Credits Reduction</em>}</li>
 * </ul>
 *
 * @see assignment.AssignmentPackage#getCourseRelation()
 * @model
 * @generated
 */
public interface CourseRelation extends EObject {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' reference.
	 * @see #setCourse(Course)
	 * @see assignment.AssignmentPackage#getCourseRelation_Course()
	 * @model
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link assignment.CourseRelation#getCourse <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Credits Reduction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits Reduction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits Reduction</em>' attribute.
	 * @see #setCreditsReduction(float)
	 * @see assignment.AssignmentPackage#getCourseRelation_CreditsReduction()
	 * @model
	 * @generated
	 */
	float getCreditsReduction();

	/**
	 * Sets the value of the '{@link assignment.CourseRelation#getCreditsReduction <em>Credits Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits Reduction</em>' attribute.
	 * @see #getCreditsReduction()
	 * @generated
	 */
	void setCreditsReduction(float value);

} // CourseRelation
