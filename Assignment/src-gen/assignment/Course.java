/**
 */
package assignment;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link assignment.Course#getName <em>Name</em>}</li>
 *   <li>{@link assignment.Course#getCode <em>Code</em>}</li>
 *   <li>{@link assignment.Course#getCreditsNumber <em>Credits Number</em>}</li>
 *   <li>{@link assignment.Course#getInstances <em>Instances</em>}</li>
 *   <li>{@link assignment.Course#getRequiredCourses <em>Required Courses</em>}</li>
 *   <li>{@link assignment.Course#getRecommendedCourses <em>Recommended Courses</em>}</li>
 *   <li>{@link assignment.Course#getContent <em>Content</em>}</li>
 *   <li>{@link assignment.Course#getCreditsReduction <em>Credits Reduction</em>}</li>
 *   <li>{@link assignment.Course#getDepartment <em>Department</em>}</li>
 * </ul>
 *
 * @see assignment.AssignmentPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see assignment.AssignmentPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link assignment.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see assignment.AssignmentPackage#getCourse_Code()
	 * @model
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link assignment.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Credits Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits Number</em>' attribute.
	 * @see #setCreditsNumber(float)
	 * @see assignment.AssignmentPackage#getCourse_CreditsNumber()
	 * @model
	 * @generated
	 */
	float getCreditsNumber();

	/**
	 * Sets the value of the '{@link assignment.Course#getCreditsNumber <em>Credits Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits Number</em>' attribute.
	 * @see #getCreditsNumber()
	 * @generated
	 */
	void setCreditsNumber(float value);

	/**
	 * Returns the value of the '<em><b>Instances</b></em>' containment reference list.
	 * The list contents are of type {@link assignment.CourseInstance}.
	 * It is bidirectional and its opposite is '{@link assignment.CourseInstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instances</em>' containment reference list.
	 * @see assignment.AssignmentPackage#getCourse_Instances()
	 * @see assignment.CourseInstance#getCourse
	 * @model opposite="course" containment="true"
	 * @generated
	 */
	EList<CourseInstance> getInstances();

	/**
	 * Returns the value of the '<em><b>Required Courses</b></em>' reference list.
	 * The list contents are of type {@link assignment.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Courses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Courses</em>' reference list.
	 * @see assignment.AssignmentPackage#getCourse_RequiredCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getRequiredCourses();

	/**
	 * Returns the value of the '<em><b>Recommended Courses</b></em>' reference list.
	 * The list contents are of type {@link assignment.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recommended Courses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recommended Courses</em>' reference list.
	 * @see assignment.AssignmentPackage#getCourse_RecommendedCourses()
	 * @model
	 * @generated
	 */
	EList<Course> getRecommendedCourses();

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see assignment.AssignmentPackage#getCourse_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link assignment.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Credits Reduction</b></em>' containment reference list.
	 * The list contents are of type {@link assignment.CourseRelation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits Reduction</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits Reduction</em>' containment reference list.
	 * @see assignment.AssignmentPackage#getCourse_CreditsReduction()
	 * @model containment="true"
	 * @generated
	 */
	EList<CourseRelation> getCreditsReduction();

	/**
	 * Returns the value of the '<em><b>Department</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link assignment.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Department</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' container reference.
	 * @see #setDepartment(Department)
	 * @see assignment.AssignmentPackage#getCourse_Department()
	 * @see assignment.Department#getCourses
	 * @model opposite="courses" transient="false"
	 * @generated
	 */
	Department getDepartment();

	/**
	 * Sets the value of the '{@link assignment.Course#getDepartment <em>Department</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department</em>' container reference.
	 * @see #getDepartment()
	 * @generated
	 */
	void setDepartment(Department value);

} // Course
