/**
 */
package assignment.impl;

import assignment.AssignmentPackage;
import assignment.Course;
import assignment.CourseInstance;
import assignment.CourseWork;
import assignment.Role;
import assignment.ScheduledHour;
import assignment.Semester;
import assignment.StudyProgram;
import assignment.Work;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getEvaluationForm <em>Evaluation Form</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getYear <em>Year</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getSemester <em>Semester</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getLabHours <em>Lab Hours</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getLectureHours <em>Lecture Hours</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getCourseWork <em>Course Work</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getStaff <em>Staff</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getStudyPrograms <em>Study Programs</em>}</li>
 *   <li>{@link assignment.impl.CourseInstanceImpl#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseInstanceImpl extends MinimalEObjectImpl.Container implements CourseInstance {
	/**
	 * The cached value of the '{@link #getEvaluationForm() <em>Evaluation Form</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationForm()
	 * @generated
	 * @ordered
	 */
	protected EList<Work> evaluationForm;

	/**
	 * The default value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected static final int YEAR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getYear() <em>Year</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYear()
	 * @generated
	 * @ordered
	 */
	protected int year = YEAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected static final Semester SEMESTER_EDEFAULT = Semester.AUTUMN;

	/**
	 * The cached value of the '{@link #getSemester() <em>Semester</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemester()
	 * @generated
	 * @ordered
	 */
	protected Semester semester = SEMESTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLabHours() <em>Lab Hours</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabHours()
	 * @generated
	 * @ordered
	 */
	protected EList<ScheduledHour> labHours;

	/**
	 * The cached value of the '{@link #getLectureHours() <em>Lecture Hours</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLectureHours()
	 * @generated
	 * @ordered
	 */
	protected EList<ScheduledHour> lectureHours;

	/**
	 * The cached value of the '{@link #getCourseWork() <em>Course Work</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseWork()
	 * @generated
	 * @ordered
	 */
	protected CourseWork courseWork;

	/**
	 * The cached value of the '{@link #getStaff() <em>Staff</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStaff()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> staff;

	/**
	 * The cached value of the '{@link #getStudyPrograms() <em>Study Programs</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyPrograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> studyPrograms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssignmentPackage.Literals.COURSE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Work> getEvaluationForm() {
		if (evaluationForm == null) {
			evaluationForm = new EObjectContainmentEList<Work>(Work.class, this,
					AssignmentPackage.COURSE_INSTANCE__EVALUATION_FORM);
		}
		return evaluationForm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getYear() {
		return year;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setYear(int newYear) {
		int oldYear = year;
		year = newYear;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.COURSE_INSTANCE__YEAR, oldYear,
					year));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Semester getSemester() {
		return semester;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemester(Semester newSemester) {
		Semester oldSemester = semester;
		semester = newSemester == null ? SEMESTER_EDEFAULT : newSemester;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.COURSE_INSTANCE__SEMESTER,
					oldSemester, semester));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScheduledHour> getLabHours() {
		if (labHours == null) {
			labHours = new EObjectContainmentEList<ScheduledHour>(ScheduledHour.class, this,
					AssignmentPackage.COURSE_INSTANCE__LAB_HOURS);
		}
		return labHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ScheduledHour> getLectureHours() {
		if (lectureHours == null) {
			lectureHours = new EObjectContainmentEList<ScheduledHour>(ScheduledHour.class, this,
					AssignmentPackage.COURSE_INSTANCE__LECTURE_HOURS);
		}
		return lectureHours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork getCourseWork() {
		return courseWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseWork(CourseWork newCourseWork, NotificationChain msgs) {
		CourseWork oldCourseWork = courseWork;
		courseWork = newCourseWork;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					AssignmentPackage.COURSE_INSTANCE__COURSE_WORK, oldCourseWork, newCourseWork);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseWork(CourseWork newCourseWork) {
		if (newCourseWork != courseWork) {
			NotificationChain msgs = null;
			if (courseWork != null)
				msgs = ((InternalEObject) courseWork).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - AssignmentPackage.COURSE_INSTANCE__COURSE_WORK, null, msgs);
			if (newCourseWork != null)
				msgs = ((InternalEObject) newCourseWork).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - AssignmentPackage.COURSE_INSTANCE__COURSE_WORK, null, msgs);
			msgs = basicSetCourseWork(newCourseWork, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.COURSE_INSTANCE__COURSE_WORK,
					newCourseWork, newCourseWork));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getStaff() {
		if (staff == null) {
			staff = new EObjectContainmentEList<Role>(Role.class, this, AssignmentPackage.COURSE_INSTANCE__STAFF);
		}
		return staff;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getStudyPrograms() {
		if (studyPrograms == null) {
			studyPrograms = new EObjectResolvingEList<StudyProgram>(StudyProgram.class, this,
					AssignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS);
		}
		return studyPrograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		if (eContainerFeatureID() != AssignmentPackage.COURSE_INSTANCE__COURSE)
			return null;
		return (Course) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourse, AssignmentPackage.COURSE_INSTANCE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		if (newCourse != eInternalContainer()
				|| (eContainerFeatureID() != AssignmentPackage.COURSE_INSTANCE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject) newCourse).eInverseAdd(this, AssignmentPackage.COURSE__INSTANCES,
						Course.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.COURSE_INSTANCE__COURSE, newCourse,
					newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AssignmentPackage.COURSE_INSTANCE__COURSE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourse((Course) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AssignmentPackage.COURSE_INSTANCE__EVALUATION_FORM:
			return ((InternalEList<?>) getEvaluationForm()).basicRemove(otherEnd, msgs);
		case AssignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			return ((InternalEList<?>) getLabHours()).basicRemove(otherEnd, msgs);
		case AssignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			return ((InternalEList<?>) getLectureHours()).basicRemove(otherEnd, msgs);
		case AssignmentPackage.COURSE_INSTANCE__COURSE_WORK:
			return basicSetCourseWork(null, msgs);
		case AssignmentPackage.COURSE_INSTANCE__STAFF:
			return ((InternalEList<?>) getStaff()).basicRemove(otherEnd, msgs);
		case AssignmentPackage.COURSE_INSTANCE__COURSE:
			return basicSetCourse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case AssignmentPackage.COURSE_INSTANCE__COURSE:
			return eInternalContainer().eInverseRemove(this, AssignmentPackage.COURSE__INSTANCES, Course.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AssignmentPackage.COURSE_INSTANCE__EVALUATION_FORM:
			return getEvaluationForm();
		case AssignmentPackage.COURSE_INSTANCE__YEAR:
			return getYear();
		case AssignmentPackage.COURSE_INSTANCE__SEMESTER:
			return getSemester();
		case AssignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			return getLabHours();
		case AssignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			return getLectureHours();
		case AssignmentPackage.COURSE_INSTANCE__COURSE_WORK:
			return getCourseWork();
		case AssignmentPackage.COURSE_INSTANCE__STAFF:
			return getStaff();
		case AssignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			return getStudyPrograms();
		case AssignmentPackage.COURSE_INSTANCE__COURSE:
			return getCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AssignmentPackage.COURSE_INSTANCE__EVALUATION_FORM:
			getEvaluationForm().clear();
			getEvaluationForm().addAll((Collection<? extends Work>) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__YEAR:
			setYear((Integer) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__SEMESTER:
			setSemester((Semester) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			getLabHours().clear();
			getLabHours().addAll((Collection<? extends ScheduledHour>) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			getLectureHours().clear();
			getLectureHours().addAll((Collection<? extends ScheduledHour>) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__COURSE_WORK:
			setCourseWork((CourseWork) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__STAFF:
			getStaff().clear();
			getStaff().addAll((Collection<? extends Role>) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			getStudyPrograms().clear();
			getStudyPrograms().addAll((Collection<? extends StudyProgram>) newValue);
			return;
		case AssignmentPackage.COURSE_INSTANCE__COURSE:
			setCourse((Course) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AssignmentPackage.COURSE_INSTANCE__EVALUATION_FORM:
			getEvaluationForm().clear();
			return;
		case AssignmentPackage.COURSE_INSTANCE__YEAR:
			setYear(YEAR_EDEFAULT);
			return;
		case AssignmentPackage.COURSE_INSTANCE__SEMESTER:
			setSemester(SEMESTER_EDEFAULT);
			return;
		case AssignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			getLabHours().clear();
			return;
		case AssignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			getLectureHours().clear();
			return;
		case AssignmentPackage.COURSE_INSTANCE__COURSE_WORK:
			setCourseWork((CourseWork) null);
			return;
		case AssignmentPackage.COURSE_INSTANCE__STAFF:
			getStaff().clear();
			return;
		case AssignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			getStudyPrograms().clear();
			return;
		case AssignmentPackage.COURSE_INSTANCE__COURSE:
			setCourse((Course) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AssignmentPackage.COURSE_INSTANCE__EVALUATION_FORM:
			return evaluationForm != null && !evaluationForm.isEmpty();
		case AssignmentPackage.COURSE_INSTANCE__YEAR:
			return year != YEAR_EDEFAULT;
		case AssignmentPackage.COURSE_INSTANCE__SEMESTER:
			return semester != SEMESTER_EDEFAULT;
		case AssignmentPackage.COURSE_INSTANCE__LAB_HOURS:
			return labHours != null && !labHours.isEmpty();
		case AssignmentPackage.COURSE_INSTANCE__LECTURE_HOURS:
			return lectureHours != null && !lectureHours.isEmpty();
		case AssignmentPackage.COURSE_INSTANCE__COURSE_WORK:
			return courseWork != null;
		case AssignmentPackage.COURSE_INSTANCE__STAFF:
			return staff != null && !staff.isEmpty();
		case AssignmentPackage.COURSE_INSTANCE__STUDY_PROGRAMS:
			return studyPrograms != null && !studyPrograms.isEmpty();
		case AssignmentPackage.COURSE_INSTANCE__COURSE:
			return getCourse() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (year: ");
		result.append(year);
		result.append(", semester: ");
		result.append(semester);
		result.append(')');
		return result.toString();
	}

} //CourseInstanceImpl
