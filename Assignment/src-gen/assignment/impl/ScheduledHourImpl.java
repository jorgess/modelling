/**
 */
package assignment.impl;

import assignment.AssignmentPackage;
import assignment.HourType;
import assignment.ScheduledHour;
import assignment.StudyProgram;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scheduled Hour</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link assignment.impl.ScheduledHourImpl#getDuration <em>Duration</em>}</li>
 *   <li>{@link assignment.impl.ScheduledHourImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link assignment.impl.ScheduledHourImpl#getBeginning <em>Beginning</em>}</li>
 *   <li>{@link assignment.impl.ScheduledHourImpl#getReservedForProgram <em>Reserved For Program</em>}</li>
 *   <li>{@link assignment.impl.ScheduledHourImpl#getDay <em>Day</em>}</li>
 *   <li>{@link assignment.impl.ScheduledHourImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScheduledHourImpl extends MinimalEObjectImpl.Container implements ScheduledHour {
	/**
	 * The default value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected static final int DURATION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDuration()
	 * @generated
	 * @ordered
	 */
	protected int duration = DURATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected static final String ROOM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected String room = ROOM_EDEFAULT;

	/**
	 * The default value of the '{@link #getBeginning() <em>Beginning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeginning()
	 * @generated
	 * @ordered
	 */
	protected static final String BEGINNING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBeginning() <em>Beginning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBeginning()
	 * @generated
	 * @ordered
	 */
	protected String beginning = BEGINNING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReservedForProgram() <em>Reserved For Program</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservedForProgram()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyProgram> reservedForProgram;

	/**
	 * The default value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected static final String DAY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected String day = DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final HourType TYPE_EDEFAULT = HourType.LAB;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected HourType type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScheduledHourImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssignmentPackage.Literals.SCHEDULED_HOUR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDuration(int newDuration) {
		int oldDuration = duration;
		duration = newDuration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.SCHEDULED_HOUR__DURATION,
					oldDuration, duration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(String newRoom) {
		String oldRoom = room;
		room = newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.SCHEDULED_HOUR__ROOM, oldRoom,
					room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBeginning() {
		return beginning;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBeginning(String newBeginning) {
		String oldBeginning = beginning;
		beginning = newBeginning;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.SCHEDULED_HOUR__BEGINNING,
					oldBeginning, beginning));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyProgram> getReservedForProgram() {
		if (reservedForProgram == null) {
			reservedForProgram = new EObjectResolvingEList<StudyProgram>(StudyProgram.class, this,
					AssignmentPackage.SCHEDULED_HOUR__RESERVED_FOR_PROGRAM);
		}
		return reservedForProgram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDay() {
		return day;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDay(String newDay) {
		String oldDay = day;
		day = newDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.SCHEDULED_HOUR__DAY, oldDay, day));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HourType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(HourType newType) {
		HourType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.SCHEDULED_HOUR__TYPE, oldType,
					type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AssignmentPackage.SCHEDULED_HOUR__DURATION:
			return getDuration();
		case AssignmentPackage.SCHEDULED_HOUR__ROOM:
			return getRoom();
		case AssignmentPackage.SCHEDULED_HOUR__BEGINNING:
			return getBeginning();
		case AssignmentPackage.SCHEDULED_HOUR__RESERVED_FOR_PROGRAM:
			return getReservedForProgram();
		case AssignmentPackage.SCHEDULED_HOUR__DAY:
			return getDay();
		case AssignmentPackage.SCHEDULED_HOUR__TYPE:
			return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AssignmentPackage.SCHEDULED_HOUR__DURATION:
			setDuration((Integer) newValue);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__ROOM:
			setRoom((String) newValue);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__BEGINNING:
			setBeginning((String) newValue);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__RESERVED_FOR_PROGRAM:
			getReservedForProgram().clear();
			getReservedForProgram().addAll((Collection<? extends StudyProgram>) newValue);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__DAY:
			setDay((String) newValue);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__TYPE:
			setType((HourType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AssignmentPackage.SCHEDULED_HOUR__DURATION:
			setDuration(DURATION_EDEFAULT);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__ROOM:
			setRoom(ROOM_EDEFAULT);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__BEGINNING:
			setBeginning(BEGINNING_EDEFAULT);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__RESERVED_FOR_PROGRAM:
			getReservedForProgram().clear();
			return;
		case AssignmentPackage.SCHEDULED_HOUR__DAY:
			setDay(DAY_EDEFAULT);
			return;
		case AssignmentPackage.SCHEDULED_HOUR__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AssignmentPackage.SCHEDULED_HOUR__DURATION:
			return duration != DURATION_EDEFAULT;
		case AssignmentPackage.SCHEDULED_HOUR__ROOM:
			return ROOM_EDEFAULT == null ? room != null : !ROOM_EDEFAULT.equals(room);
		case AssignmentPackage.SCHEDULED_HOUR__BEGINNING:
			return BEGINNING_EDEFAULT == null ? beginning != null : !BEGINNING_EDEFAULT.equals(beginning);
		case AssignmentPackage.SCHEDULED_HOUR__RESERVED_FOR_PROGRAM:
			return reservedForProgram != null && !reservedForProgram.isEmpty();
		case AssignmentPackage.SCHEDULED_HOUR__DAY:
			return DAY_EDEFAULT == null ? day != null : !DAY_EDEFAULT.equals(day);
		case AssignmentPackage.SCHEDULED_HOUR__TYPE:
			return type != TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (duration: ");
		result.append(duration);
		result.append(", room: ");
		result.append(room);
		result.append(", beginning: ");
		result.append(beginning);
		result.append(", day: ");
		result.append(day);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //ScheduledHourImpl
