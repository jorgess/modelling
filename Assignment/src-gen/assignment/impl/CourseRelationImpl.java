/**
 */
package assignment.impl;

import assignment.AssignmentPackage;
import assignment.Course;
import assignment.CourseRelation;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Relation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link assignment.impl.CourseRelationImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link assignment.impl.CourseRelationImpl#getCreditsReduction <em>Credits Reduction</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseRelationImpl extends MinimalEObjectImpl.Container implements CourseRelation {
	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected Course course;

	/**
	 * The default value of the '{@link #getCreditsReduction() <em>Credits Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditsReduction()
	 * @generated
	 * @ordered
	 */
	protected static final float CREDITS_REDUCTION_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getCreditsReduction() <em>Credits Reduction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreditsReduction()
	 * @generated
	 * @ordered
	 */
	protected float creditsReduction = CREDITS_REDUCTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AssignmentPackage.Literals.COURSE_RELATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		if (course != null && course.eIsProxy()) {
			InternalEObject oldCourse = (InternalEObject) course;
			course = (Course) eResolveProxy(oldCourse);
			if (course != oldCourse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AssignmentPackage.COURSE_RELATION__COURSE,
							oldCourse, course));
			}
		}
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course basicGetCourse() {
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		Course oldCourse = course;
		course = newCourse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.COURSE_RELATION__COURSE, oldCourse,
					course));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getCreditsReduction() {
		return creditsReduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreditsReduction(float newCreditsReduction) {
		float oldCreditsReduction = creditsReduction;
		creditsReduction = newCreditsReduction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AssignmentPackage.COURSE_RELATION__CREDITS_REDUCTION,
					oldCreditsReduction, creditsReduction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AssignmentPackage.COURSE_RELATION__COURSE:
			if (resolve)
				return getCourse();
			return basicGetCourse();
		case AssignmentPackage.COURSE_RELATION__CREDITS_REDUCTION:
			return getCreditsReduction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AssignmentPackage.COURSE_RELATION__COURSE:
			setCourse((Course) newValue);
			return;
		case AssignmentPackage.COURSE_RELATION__CREDITS_REDUCTION:
			setCreditsReduction((Float) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AssignmentPackage.COURSE_RELATION__COURSE:
			setCourse((Course) null);
			return;
		case AssignmentPackage.COURSE_RELATION__CREDITS_REDUCTION:
			setCreditsReduction(CREDITS_REDUCTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AssignmentPackage.COURSE_RELATION__COURSE:
			return course != null;
		case AssignmentPackage.COURSE_RELATION__CREDITS_REDUCTION:
			return creditsReduction != CREDITS_REDUCTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (creditsReduction: ");
		result.append(creditsReduction);
		result.append(')');
		return result.toString();
	}

} //CourseRelationImpl
