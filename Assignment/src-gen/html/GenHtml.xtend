package html

import java.io.IOException
import assignment.Course
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import assignment.AssignmentPackage
import assignment.Department
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl
import assignment.util.AssignmentResourceFactoryImpl
import assignment.CourseInstance
import assignment.Role
import assignment.ScheduledHour
import assignment.Work
import java.util.Arrays
import java.io.PrintStream
import assignment.CourseRelation

class GenHtml {
	
	def CharSequence generateHtml(Course course, StringBuilder stringBuilder) {
		generatePreHtml(course.code, course.name, stringBuilder)
		generateContent(course, stringBuilder);
		testSomething(course, stringBuilder);
		generatePostHtml(stringBuilder);
	}
	
	def CharSequence generatePreHtml(String code, String name, StringBuilder stringBuilder) {
		stringBuilder.append('''
<!DOCTYPE html>
<html>
<head>
	<title>�code� - �name�</title>
	<meta charset="utf-8"/>
	<style>
	table, th, td {
	    border: 1px solid black;
	}
	</style>	
</head>
<body>
''');
	}
	
	def CharSequence generatePostHtml(StringBuilder stringBuilder) {
		stringBuilder.append("\n</body></html>");
	}
	
	def static Course getCourse(String uriString) throws IOException {
		val resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(AssignmentPackage.eNS_URI, AssignmentPackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new AssignmentResourceFactoryImpl());
		val resource = resSet.getResource(URI.createURI(uriString), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof Department) {
				return eObject.courses.get(0);
			}
		}
		return null;
	}


	def CharSequence generateContent(Course course, StringBuilder stringBuilder) {
		stringBuilder.append('''
			<h1>�course.code� - �course.name�</h1>
			<h2>Content</h2>
			�course.content�
		''');
		for (CourseInstance ci : course.instances) {
			stringBuilder.append('''
				<h2>�ci.semester� - �ci.year�</h2>  
			''')
			generateEvaluationForm(ci, stringBuilder);
			generateTimetable(ci, stringBuilder);
			generateStaff(ci, stringBuilder);
		}
		stringBuilder.append('''''');
	}
	
	def CharSequence generateEvaluationForm(CourseInstance courseInstance, StringBuilder stringBuilder) {
		stringBuilder.append('''
			<h3>Examination arrangement</h3>
			<table>
			       <tr>
			           <th>Evaluation Form</th>
			           <th>Weighting</th>
			       </tr>
		''')
		for (Work w : courseInstance.evaluationForm) {
			stringBuilder.append('''
				<tr>
					<td>�w.type�</td>
					<td>�w.percentage�/100</td>
				</tr>
			''')
		}
		stringBuilder.append('''
		</table>''')
	}
	
	def CharSequence testSomething(Course course, StringBuilder stringBuilder) {
		stringBuilder.append("<h3>Point reductions</h3>")
		stringBuilder.append('''
		<table>
			<tr>
				<th> Course </th>
				<th> Reduction </th>
			</tr>
			'''
		)
		for (CourseRelation relation : course.creditsReduction) {
			stringBuilder.append('''
			<tr>
				<td>�relation.course.name�</td>
				<td>�relation.creditsReduction�</td>
			</tr> 
			''')
		}
		stringBuilder.append("</table>")
		return stringBuilder;
	}

	def CharSequence generateTimetable(CourseInstance courseInstance, StringBuilder stringBuilder) {
		stringBuilder.append('''
			<h3>Timetable</h3>
			<table>
			       <tr>
			           <th>Day</th>
			           <th>Type</th>
			           <th>Room</th>
			           <th>Time</th>
			           <th>Length</th>
			       </tr>
		''')
		for (ScheduledHour lh : courseInstance.labHours) {
			stringBuilder.append('''
				<tr>
					<td>�lh.day�</td>
					<td>�lh.type�</td>
					<td>�lh.room�</td>
					<td>�lh.beginning�</td>
					<td>�lh.duration�</td>
				</tr>
			''')
		}
		for (ScheduledHour lh : courseInstance.lectureHours) {
			stringBuilder.append('''
				<tr>
					<td>�lh.day�</td>
					<td>�lh.type�</td>
					<td>�lh.room�</td>
					<td>�lh.beginning�</td>
					<td>�lh.duration�</td>
				</tr>
			''')
		}
		stringBuilder.append('''
		</table>''')
	}

	def CharSequence generateStaff(CourseInstance courseInstance, StringBuilder stringBuilder) {
				stringBuilder.append('''
			<h3>Contact information</h3>
			Department with academic responsibility : 
			�courseInstance.course.department.name�
			<table>
			       <tr>
			           <th>Name</th>
			           <th>Email</th>
			           <th>Role</th>
			       </tr>
		''')
		for (Role r : courseInstance.staff) {
			stringBuilder.append('''
				<tr>
					<td>�r.people.name�</td>
					<td>�r.people.email�</td>
					<td>�r.type�</td>
				</tr>
			''')
		}
		stringBuilder.append('''
		</table>''')
	}
	
	def static void main(String[] args) throws IOException {		
		val argsAsList = Arrays.asList(args)
		val course = if(argsAsList.size > 0) getCourse(argsAsList.get(0)) else getCourse(GenHtml.getResource("Department.xmi").toString());
		val html = new GenHtml().generateHtml(course, new StringBuilder);
		if (args.length > 1) {
			val target = URI.createURI(argsAsList.get(1));
			val ps = new PrintStream(course.eResource().getResourceSet().getURIConverter().createOutputStream(target))
			ps.print(html);
		} else {
			System.out.println(html);
		}
	}
}